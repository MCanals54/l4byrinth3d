# L4byrinth3D

Projet Labyrinthe 3D d'élèves de terminale NSI.<br>
- Ambroise GILBERT
- Cyprien ROUCHON

Lycée Henri Loritz 
29, rue de jardiniers,
54000 Nancy
---


## Description du projet

Il s'agit d'une modélisation 3D d'un Labyrinthe généré aléatoirement d'une taille définie par l'utilisateur dans l'interface et dans lequel il peut se déplacer librement. Le labyrinthe possède toujours une case de sortie qui est indiquée en rouge. Il est également possible de jouer en multijoueur,  où vous pouvez vous déplacer ensemble dans le même labyrinthe. Enfin il est possible de jouer en mode PvP (player versus player).

Il est également possible d'afficher la modélisation 2D ce qui permet de vérifier qu'il n'y pas de problème avec l'affichage 3D.

## Prérequis

Pour pouvoir lancer le projet il est nécessaire d'avoir téléchargé au préalable les librairies présentes dans requirements.txt .

## Mode d'emploi

Pour lancer le programme il vous suffit d'exécuter le fichier `main.py`.
Puis il faut choisir le mode solo (tout seul) ou le mode multijoueur (à plusieurs).
Le déplacement se fait avec les touches `Z` (avancer) et `X` (reculer) `Q` et `D` pour des translations gauche et droite. On utilise la souris pour orienter le joueur.<br>
**Pour quitter le jeu en cours il faut appuyer sur `ech`**
- si le joueur choisi de jouer seul, il lui faudra choisir la taille du labyrinthe et si oui ou non il veut afficher la version (pour le débogage)
- si le joueur choisi le mode multijoueur, il lui faut choisir s'il veut créer un serveur ou en rejoindre un
        
    - si le joueur crée le serveur, il lui faudra choisir la taille du labyrinthe, s'il affiche le mode 2D (option de débogage), s'il veut jouer en PvP(player versus player), ainsi que le pseudo qu'il aura dans le jeu, le nom du serveur et le nombre de joueurs.

    - si le joueur rejoint un serveur existant, il lui faudra choisir le pseudo qu'il aura dans le jeu, le nom du serveur qu'il veut rejoindre et s'il veut afficher la version 2D (option de débogage).

## Note

lorsque l'on rejoint une partie il faut bien attendre que le serveur ait fini de se lancer, le fait que celui qui crée la partie soit sur l'ecran d'attente ne veut pas necessairement dire que le serveur est bien initialisé

De plus, certaines configurations réseaux sont incompatibles avec le mode multijoueur, par exemple sur la connexion du lycée il nous était impossible de faire marcher les scripts à cause du proxy qui ne laisse pas passer les requêtes.
Certaines erreurs peuvent aussi être liées à une connexion trop instable ou trop lente.

Le projet n'a pas été testé sur linux mais rien n'indique que ça ne marcherait pas sur cet os

Le mode pvp(player versus player) comporte encore quelques bugs tels que le comptage des points ou la possibilité qu'un joueur se retrouve enfermé lorsqu'il réapparait après s'être fait éliminé.
