import pygame
from time import sleep, perf_counter
from fnRaycast import raycast
from convert3D import verticalRect
from divers import *
from tkinter import *
from classe_joueur import *
from createLab import *
from skin import skin_pattern
from random import randint
import os 

#chargement des resources (images, sons, etc.)

dir_path = os.path.dirname(os.path.realpath(__file__))

dir_order = ["nord", "ouest", "sud", "est"]
CODE_COULEUR = ["green", "blue", "red", "purple"]
start, runGame, frameSettings, frameEnd, text_fontV = [None] * 5
echap = False
reloading = False
widgets = {}
showSettings, showEnd, wF, hF = False, False, None, None
cooldown = {"echap": 0, "end": 0}
last_clic = perf_counter()

img_gun = pygame.image.load(dir_path+"\\src\\gun.png")
img_shooting = pygame.image.load(dir_path+"\\src\\shooting.png")
img_bullets = pygame.image.load(dir_path+"\\src\\bullets.png")

xy_gun = [0, 0]
pygame.mixer.init()
sound_gun = pygame.mixer.Sound(dir_path+"\\src\\gun.mp3")
sound_empty_gun = pygame.mixer.Sound(dir_path+"\\src\\empty_gun.mp3")
sound_reload = pygame.mixer.Sound(dir_path+"\\src\\reload.mp3")
sound_dead = pygame.mixer.Sound(dir_path+"\\src\\dead.mp3")

def init(widgetStuff, HEIGHT):
    """
    initialise toutes les variables globales utilisées ensuite dans draw_lab
    :param widgetStuff: dict, contient des éléments d'affichage
    :param HEIGHT: hauteur de l'écran
    """
    global widgets, wF, hF, frameEnd, text_fontV, frameSettings, img_gun, img_shooting
    widgets, wF, hF, frameEnd, text_fontV, frameSettings = widgetStuff["widgets"], widgetStuff["wF"], widgetStuff["hF"], widgetStuff["frameEnd"], widgetStuff["text_fontV"], widgetStuff["frameSettings"]
    img_gun = pygame.transform.scale(img_gun, (HEIGHT/22*5, HEIGHT/22*7))
    img_shooting = pygame.transform.scale(img_shooting, (HEIGHT/10, HEIGHT/10))

def reload(joueur, HEIGHT):
    """
    permet de recharger l'arme d'un joueur
    :param joueur: objet joueur
    :param HEIGHT: hauteur de la fenêtre (l'écran)
    """
    global xy_gun, reloading
    sound_reload.play()
    vitesse = (HEIGHT/22*6.5)/50
    xy_gun = [0, 0]
    for i in range(50):
        xy_gun[1] += vitesse
        sleep(0.015)

    sleep(0.5)

    for i in range(50):
        xy_gun[1] -= vitesse
        sleep(0.015)

    joueur.gun.reload()
    reloading = False

def drawLab(joueur,WIDTH,HEIGHT,screen,grilleLab,case_sortie, multi=True, autres_joueurs={}, server=None, FS = True, PVP=False):
    """La fonction dessine tout ce qui s'affiche à l'écran: les murs en 3D, les autres joueurs (+ la représentation 2D si FS == False)
        :param joueur: le joueur (type: objet de la classe joueur)
        :param WIDTH: largeur de l'écran (type: int)
        :param HEIGHT: largeur de l'écran (type: int)
        :param screen: objet Surface (de pygame) où tout est dessiné
        :param grilleLab: la grille du labyrinthe dans le format décrit et renvoyé dans la fonction genererGrilleLab() (createLab.py)
        :param case_sortie: tuple d'entier faisant référence à la case de sortie du labyrinthe
        :param multi: La partie est en mode solo (False) ou en multijoueur (True)
        :param autres_joueurs: dictionnaire des autres joueurs (s'il y'en a), la clé de chaque joueur est son id
        :param server: objet Server (uttile si la partie est en multijoueur, permet l'envoie des informations du joueur)
        :param FS: affichage sur le plein écran (True) ou affichage sur la deuxième moitié de l'écran avec en plus la représentation 2D (False)
        :return: void
    """
    global echap, widgets, wF, hF, frameEnd, text_fontV, frameSettings, showEnd, showSettings, last_clic, reloading
    

    pygame.draw.rect(screen, pygame.Color(22,10,15), (0,0,WIDTH,HEIGHT)) # pour effacer la frame précédente et appliquer un fond noir

    keys={"z": K_z, "q": K_q, "s": K_s, "d": K_d, "echap": K_ESCAPE, "r": K_r}
    key_p = key.get_pressed()

    msg = ""
    shooting = False

    # si le joueur est mort, il réapparaît à un endroit aléatoire de la map (un minimum éloigné de là où il est mort)
    if joueur.mort():
        sound_dead.play()
        size = len(grilleLab[0])
        trouve = False
        while not trouve:
            newCase = (randint(0, size-1), randint(0, size-1))
            if size < 6 or dist((joueur.x, joueur.y), newCase) > 4: trouve = True
        joueur.vie = 100
        joueur.x = newCase[0] * 200 +30
        joueur.y = newCase[1] * 200 +30
        server.send(f",m,{joueur.id},{joueur.x},{joueur.y},v")
        

    if True in key_p: # gère les actions du joueurs avec les touches
        if key_p[keys["z"]]: 
            joueur.avancer(autres_joueurs)
            if not reloading: xy_gun[1] += 0.05
        if key_p[keys["s"]]: 
            joueur.reculer(autres_joueurs)
            if not reloading: xy_gun[1] -= 0.05
        if key_p[keys["q"]]: 
            joueur.gauche(autres_joueurs)
            if not reloading: xy_gun[0] += 0.05
        if key_p[keys["d"]]: 
            joueur.droite(autres_joueurs)
            if not reloading: xy_gun[0] -= 0.05
        if PVP and key_p[keys["r"]] and not echap and not reloading and joueur.gun.nb_bullets < joueur.gun.nb_bullets_max :
            reloading = True
            creer_un_thread(lambda: reload(joueur, HEIGHT))
       
        if key_p[keys["echap"]]:
            if cooldown["echap"] == 0:
                echap = not echap
                cooldown["echap"] = 20
                mouse.set_visible(echap)
                set_show_settings()

        msg +=  "m"

    mouseX, mouseY = mouse.get_pos()
    
    # gère la vision en fonction de la position de la souris
    if not echap:
        if mouseX < WIDTH/2: 
            joueur.tourner_gauche(WIDTH/2 - mouseX)
            msg +=  "o"
        if mouseX > WIDTH/2: 
            joueur.tourner_droite(mouseX - WIDTH/2)
            msg +=  "o"
        if mouseY < HEIGHT/2: joueur.tourner_haut(HEIGHT/2 - mouseY)
        if mouseY > HEIGHT/2: joueur.tourner_bas(mouseY - HEIGHT/2)
        mouse.set_pos((WIDTH/2, HEIGHT/2)) # on repositione la souris au milieu de l'écran


    if PVP and pygame.mouse.get_pressed()[0] and perf_counter()-last_clic > 1 and not reloading and not echap: # gère le tire
        if not joueur.gun.isLoaded(): sound_empty_gun.play()
        else: 
            shooting = True
            sound_gun.play()
            last_clic = perf_counter()
            impact = joueur.tirer(autres_joueurs)
            degat = None
            if len(impact) == 3: #si on touche un joueur
                coo,hauteur,id = impact
                j = autres_joueurs[id]
                if 0 <= hauteur <= 0.4*j.size:
                    print("jambe")
                    degat = 25
                if 0.4*j.size < hauteur <= 0.85*j.size:
                    print("abdomem")
                    degat = 40
                if 0.85*j.size < hauteur <= j.size:
                    print("tete")
                    degat = 80

                if degat != None:
                    j.degat(degat)
                    if j.mort():
                        server.send(f"k,{joueur.id},")
                        print("envoie kill")
                        joueur.nbEliminations +=1

            if degat == None: #si le tire n'a pas touché de joueur
                server.send(f"t,{joueur.x},{joueur.y},v,v")
                print("envoie tire")
            else:
                server.send(f'd,{joueur.x},{joueur.y},{id},{degat}')
                print("envoie degat")

    if multi: # envoie des deplacements
        if msg == "m":
            server.send(f",m,{joueur.id},{joueur.x},{joueur.y},v")
        if msg == "o":
            server.send(f",o,{joueur.id},{joueur.orientationHorizontale},v,v")
        if msg == "mo":
            server.send(f",mo,{joueur.id},{joueur.x},{joueur.y},{joueur.orientationHorizontale}")

    x,y,oriH = joueur.x, joueur.y, joueur.orientationHorizontale
    if not FS: ratio = (HEIGHT/2)/(len(grilleLab[0])*200)
    else: ratio = None

    # dessiner le ciel
    horizonY = HEIGHT/2 + (joueur.orientationVerticale/90) * HEIGHT
    if FS: pygame.draw.rect(screen, pygame.Color(60, 150, 220), pygame.Rect(0, 0, WIDTH, horizonY))
    else: pygame.draw.rect(screen, pygame.Color(60, 150, 220), pygame.Rect(0, HEIGHT, WIDTH, horizonY))

    listeMurs = raycast((x, y), screen, oriH-45, grilleLab, nb_rayon= 90, ecart=1.05, ratio=ratio) # permet de récupérer les murs qui sont dans le champ de vision du joueur dans l'ordre du plus éloigné au plus proche

    # En fonction de la distance on incruste dans listeMurs les joueurs avec leur ID 
    if multi:
        list_affichage = []
        casesJoueurs = []
        id_joueurs = [int(jID) for jID in list(autres_joueurs.keys())]

        casesJoueurs = [coo_to_case(autres_joueurs[jID].x, autres_joueurs[jID].y) for jID in id_joueurs]

        for m in listeMurs:
            joueurs_a_retirer = []
            for jID in id_joueurs:
                j = autres_joueurs[jID]
                if m[2] > dist((joueur.x,joueur.y),(j.x,j.y)):
                    list_affichage.append((m[0], m[1]))
                else:
                    list_affichage.append(jID)
                    list_affichage.append((m[0], m[1]))
                    joueurs_a_retirer.append(jID)
            if len(id_joueurs) == 0:
                list_affichage.append((m[0], m[1]))

            for j in joueurs_a_retirer: id_joueurs.remove(j)
                
        
        for jID in id_joueurs: list_affichage.append(jID)

        listeMurs = list_affichage
        
        casesMurs = [m[1] for m in listeMurs if type(m)!=int]

        # Ici on fait quelques ajustements pour la position des joueurs dans l'ordre où il fait dessiner les éléments
        # Le but est d'éviter qu'un mur apparaisse devant un joueur là où li ne faudrait pas

        for i,jID in enumerate(autres_joueurs):
            j = autres_joueurs[jID]
            if casesJoueurs[i] in casesMurs:

                cases = [c for c in casesMurs if c == casesJoueurs[i]]
                for c in cases:
                    indexJ = indexM = None
                    for cote in ["nord", "ouest", "sud", "est"]:
                        if (cote, c) in listeMurs :
                            indexM = listeMurs.index((cote, c))
                            indexJ = listeMurs.index(j.id)
                            if indexM != None and indexJ < indexM:
                                print(f"Avec le joueur {CODE_COULEUR[jID]}, il y'a {indexM - indexJ} décalage(s) (avec le mur {cote})")
                                for i in range(indexM - indexJ): listeMurs[indexJ+i], listeMurs[indexJ+i+1] = listeMurs[indexJ+i+1], listeMurs[indexJ+i]


    # On créer les 4 "rectangles" qui composent chaque murs avec les coordonnées par rapport aux cases qui sont converties en coodonnées "absolues"
    murs = cooAbsoluesMurs(listeMurs, case_sortie)

    x_case, y_case = coo_to_case(x,y)
    if not PVP: dans_case_sortie = y_case == case_sortie[0] and x_case == case_sortie[1]
    
    # dessin du joueur en 2D et de son champ de vision (si le labyrinthe n'est pas en plein écran et que donc on souhaite afficher la représentation 2D)
    if not FS:
        joueur.drawPlayer2D(screen, ratio=ratio)
        for m in murs: 
            if type(m) != int: pygame.draw.line(screen, start_pos=(m["x1"]*ratio, m["y1"]*ratio), end_pos=(m["x2"]*ratio, m["y2"]*ratio), color="white") # dessin des murs 2D
        if multi: # dessin des autres joueurs en 2D
            for j in autres_joueurs: autres_joueurs[j].drawPlayer2D(screen, col=CODE_COULEUR[autres_joueurs[j].id], ratio=ratio)

    # représentation 3D de la vision du joueur
    for m in murs:
        if type(m) == int:
            j = autres_joueurs[m]
            j_dessin = skin_pattern(j, 2)

            for jside in j_dessin[0:-4]:
                pt1 = rotation((jside["x1"], jside["y1"]), (j.x,j.y), j.orientationHorizontale)
                pt2 = rotation((jside["x2"], jside["y2"]), (j.x,j.y), j.orientationHorizontale)
                side = verticalRect(joueur, WIDTH, HEIGHT, FS, (pt1[0], pt1[1], jside["z1"]), (pt2[0], pt2[1], jside["z2"]))
                
                if side["draw"] and abs(side["x1"] - side["x2"]) < 0.8*WIDTH and abs(side["y1"] - side["y3"]) < 0.95*HEIGHT and abs(side["y2"] - side["y4"]) < 0.95*HEIGHT:
                    if jside["contour"]: pygame.draw.polygon(screen, "black", ((side["x1"], side["y1"]), (side["x2"], side["y2"]), (side["x2"], side["y4"]), (side["x1"], side["y3"])), width=5)
                    pygame.draw.polygon(screen, CODE_COULEUR[m], ((side["x1"], side["y1"]), (side["x2"], side["y2"]), (side["x2"], side["y4"]), (side["x1"], side["y3"])))
        
            #afficher le pseudo
            #on cherche à trouver le cadre le plus approprié où afficher le pseudo
            #celui-ci permet de définir également la taille du texte pour le pseudo
            best_size = None
            best_side = None
            for i in range(4):
                text_cadre = j_dessin[-i]
                side = verticalRect(joueur, WIDTH, HEIGHT, FS, (text_cadre['x1'], text_cadre['y1'], text_cadre["z1"]), (text_cadre['x2'], text_cadre['y2'], text_cadre["z2"]))
                if side["draw"] and (best_size == None or int(abs(side["x2"] - side["x1"])) > best_size): 
                    best_size = int(abs(side["x2"] - side["x1"]))
                    best_side = side

            if best_size != None: 
                text_fontPseudo = pygame.font.SysFont("sans-serif", best_size)
                draw_text(screen, j.pseudo, text_fontPseudo, "white", int(best_side["x1"] - (best_size/10*len(j.pseudo))), best_side["y1"])
                
        else:
            mur = verticalRect(joueur, WIDTH, HEIGHT, FS, (m["x1"], m["y1"], 0), (m["x2"], m["y2"], 200))
            if mur["draw"]: 
                m_color = pygame.Color(50, 68, 60)
                if m["sortie"]: m_color = "red"
                pygame.draw.polygon(screen, "black", ((mur["x1"], mur["y1"]), (mur["x2"], mur["y2"]), (mur["x2"], mur["y4"]), (mur["x1"], mur["y3"])), width=5)
                pygame.draw.polygon(screen, m_color, ((mur["x1"], mur["y1"]), (mur["x2"], mur["y2"]), (mur["x2"], mur["y4"]), (mur["x1"], mur["y3"])))

    if not PVP and dans_case_sortie and cooldown["end"] == 0: showEnd = True

    if not echap and PVP:
        pygame.draw.line(screen,'red',(WIDTH/2 -15, HEIGHT/2),(WIDTH/2 +15, HEIGHT/2), 3)
        pygame.draw.line(screen,'red',(WIDTH/2, HEIGHT/2 -15),(WIDTH/2, HEIGHT/2 +15), 3) 

        draw_text(screen, f"- {joueur.pseudo} (vous): {joueur.nbEliminations} points", pygame.font.SysFont("verdana", int(WIDTH/50)), "white", 50, 0.4*HEIGHT)
        for i, jID in enumerate(autres_joueurs.keys()):
            j = autres_joueurs[jID]
            draw_text(screen, f"- {j.pseudo}: {j.nbEliminations} points", pygame.font.SysFont("verdana", int(WIDTH/50)), "white", 50, 0.4*HEIGHT + ((i+1)*0.05 * HEIGHT))

    if PVP:
        # afficher la barre de vie
        if FS:
            draw_text(screen, f"Votre vie: {joueur.vie}/100", pygame.font.SysFont("verdana", int(WIDTH/45)), "white", WIDTH/2.7, 20)
            pygame.draw.rect(screen, "red", Rect(WIDTH/3, 100, WIDTH/3, 10))
            pygame.draw.rect(screen, "green", Rect(WIDTH/3, 100, WIDTH/3 * joueur.vie/100, 10))
        else:
            draw_text(screen, f"Votre vie: {joueur.vie}/100", pygame.font.SysFont("verdana", int(WIDTH/45)), "white", WIDTH/2.7, HEIGHT + 20)
            pygame.draw.rect(screen, "red", Rect(WIDTH/3, HEIGHT + 50, WIDTH/3, 10))
            pygame.draw.rect(screen, "green", Rect(WIDTH/3, HEIGHT + 50, WIDTH/3 * joueur.vie/100, 10))

        draw_text(screen, f"{joueur.gun.nb_bullets}/{joueur.gun.nb_bullets_max}", pygame.font.SysFont("verdana", 35), "white" if joueur.gun.nb_bullets>0 else "red", WIDTH - 190, HEIGHT - 200)
        screen.blit(img_bullets, (WIDTH - 200, HEIGHT - 150))
        hauteur_gun = sin(xy_gun[1])*30 if not reloading else xy_gun[1]
        screen.blit(img_gun, (WIDTH - (HEIGHT/22*12) + cos(xy_gun[0])*30, HEIGHT - (HEIGHT/22*6.5) + hauteur_gun))
        if shooting: screen.blit(img_shooting, (WIDTH - (HEIGHT/22*12) + cos(xy_gun[0])*30, HEIGHT - (HEIGHT/22*7.5) + sin(xy_gun[1])*30))


    if not PVP and not dans_case_sortie and showEnd: 
        showEnd = False
        mouse.set_visible(False)
        echap = False

    if not PVP and showEnd and not showSettings:
        echap = True
        mouse.set_visible(True)
        screen.blit(frameEnd, ((WIDTH-wF*0.7)/2, (HEIGHT-hF*0.7)/2))
        widgets["buttonContinue"].show()
        widgets["buttonEnd"].show()
    elif not PVP: 
        widgets["buttonContinue"].hide()
        widgets["buttonEnd"].hide()

    if showSettings:
        screen.blit(frameSettings, ((WIDTH-wF)/2, (HEIGHT-hF)/2))
        sliderV = int(widgets["sensi"].getValue())
        widgets["sensiOutput"].setText(str(sliderV/100))
        joueur.sensi = sliderV/10

    for k in cooldown.keys():
        if cooldown[k] > 0: cooldown[k] -= 0.5
        if cooldown[k] < 0: cooldown[k] = 0


def set_show_settings():
    """
    affiche/cache les parametres (la sensibilité et le bouton pour quiter la partie) en fonction de s'il sont déjà respectivement caché/affiché
    """
    global showSettings, widgets, wF, hF, cooldown

    showSettings = not showSettings

    if showSettings:
        widgets["sensi"].show()
        widgets["sensiOutput"].show()
        widgets["back"].show()
        widgets["exit"].show()
    else:
        widgets["sensi"].hide()
        widgets["sensiOutput"].hide()
        widgets["back"].hide()
        widgets["exit"].hide()


def function_back():
    """
    permet de retourner dans le jeu (enlève les parametres ou enlève la fenêtre de fin)
    """
    global echap, showEnd    
    if not showEnd: set_show_settings()
    else:  
        showEnd = False
        cooldown["end"] = 300
    echap = False
    mouse.set_visible(False)

