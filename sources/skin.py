def skin_pattern(j, nb=1):
    """
    skin_pattern renvoie un modèle de représentation 3D pour dessiner un joueur
    cette représentation se compose d'une liste de dictionnaires qui correspondent à des rectangles verticaux dans le plan 3D
    :param nb: entier correspond au modèle souhaité (pour l'instant il n'y en a que 2)
    return la représention du joueur sous forme de liste de dictionnaires
    """
    if nb == 1: return [
        {'x1': j.x - 3, 'y1': j.y - 3, 'z1': 0, 'x2': j.x - 3, 'y2': j.y + 3, 'z2': j.size, 'contour': True},
        {'x1': j.x + 3, 'y1': j.y - 3, 'z1': 0, 'x2': j.x + 3, 'y2': j.y + 3, 'z2': j.size, 'contour': True},
        {'x1': j.x - 3, 'y1': j.y - 3, 'z1': 0, 'x2': j.x + 3, 'y2': j.y - 3, 'z2': j.size, 'contour': True},
        {'x1': j.x - 3, 'y1': j.y + 3, 'z1': 0, 'x2': j.x + 3, 'y2': j.y + 3, 'z2': j.size, 'contour': True},
        #cadre pour le pseudo
        {'x1': j.x - 4, 'y1': j.y - 4, 'z1': 1.15*j.size, 'x2': j.x + 4, 'y2': j.y - 4, 'z2': 1.25*j.size},
        {'x1': j.x - 4, 'y1': j.y + 4, 'z1': 1.15*j.size, 'x2': j.x + 4, 'y2': j.y + 4, 'z2': 1.25*j.size},
        {'x1': j.x - 4, 'y1': j.y - 4, 'z1': 1.15*j.size, 'x2': j.x - 4, 'y2': j.y + 4, 'z2': 1.25*j.size},
        {'x1': j.x + 4, 'y1': j.y - 4, 'z1': 1.15*j.size, 'x2': j.x + 4, 'y2': j.y + 4, 'z2': 1.25*j.size}
    ]
    
    if nb == 2: return [
        #jambe gauche
        {'x1': j.x - 6, 'y1': j.y - 3, 'z1': 0, 'x2': j.x - 6, 'y2': j.y + 3, 'z2': 0.4*j.size, 'contour': True},
        {'x1': j.x - 3, 'y1': j.y - 3, 'z1': 0, 'x2': j.x - 3, 'y2': j.y + 3, 'z2': 0.4*j.size, 'contour': True},
        {'x1': j.x - 6, 'y1': j.y - 3, 'z1': 0, 'x2': j.x - 3, 'y2': j.y - 3, 'z2': 0.4*j.size, 'contour': True},
        {'x1': j.x - 6, 'y1': j.y + 3, 'z1': 0, 'x2': j.x - 3, 'y2': j.y + 3, 'z2': 0.4*j.size, 'contour': True},
        #jambe droite
        {'x1': j.x + 6, 'y1': j.y - 3, 'z1': 0, 'x2': j.x + 6, 'y2': j.y + 3, 'z2': 0.4*j.size, 'contour': True},
        {'x1': j.x + 3, 'y1': j.y - 3, 'z1': 0, 'x2': j.x + 3, 'y2': j.y + 3, 'z2': 0.4*j.size, 'contour': True},
        {'x1': j.x + 6, 'y1': j.y - 3, 'z1': 0, 'x2': j.x + 3, 'y2': j.y - 3, 'z2': 0.4*j.size, 'contour': True},
        {'x1': j.x + 6, 'y1': j.y + 3, 'z1': 0, 'x2': j.x + 3, 'y2': j.y + 3, 'z2': 0.4*j.size, 'contour': True},
        #ventre/dos
        {'x1': j.x - 6, 'y1': j.y - 4, 'z1': 0.4*j.size, 'x2': j.x - 6, 'y2': j.y + 4, 'z2': 0.85*j.size, 'contour': True},
        {'x1': j.x + 6, 'y1': j.y - 4, 'z1': 0.4*j.size, 'x2': j.x + 6, 'y2': j.y + 4, 'z2': 0.85*j.size, 'contour': True},
        {'x1': j.x - 6, 'y1': j.y - 4, 'z1': 0.4*j.size, 'x2': j.x + 6, 'y2': j.y - 4, 'z2': 0.85*j.size, 'contour': True},
        {'x1': j.x - 6, 'y1': j.y + 4, 'z1': 0.4*j.size, 'x2': j.x + 6, 'y2': j.y + 4, 'z2': 0.85*j.size, 'contour': True},
        #épaule gauche
        {'x1': j.x - 10, 'y1': j.y - 4, 'z1': 0.75*j.size, 'x2': j.x - 10, 'y2': j.y + 4, 'z2': 0.85*j.size, 'contour': False},
        {'x1': j.x - 6, 'y1': j.y - 4, 'z1': 0.75*j.size, 'x2': j.x - 6, 'y2': j.y + 4, 'z2': 0.85*j.size, 'contour': False},
        {'x1': j.x - 10, 'y1': j.y - 4, 'z1': 0.75*j.size, 'x2': j.x - 6, 'y2': j.y - 4, 'z2': 0.85*j.size, 'contour': False},
        {'x1': j.x - 10, 'y1': j.y + 4, 'z1': 0.75*j.size, 'x2': j.x - 6, 'y2': j.y + 4, 'z2': 0.85*j.size, 'contour': False},
        #épaule droite
        {'x1': j.x + 10, 'y1': j.y - 4, 'z1': 0.75*j.size, 'x2': j.x + 10, 'y2': j.y + 4, 'z2': 0.85*j.size, 'contour': False},
        {'x1': j.x + 6, 'y1': j.y - 4, 'z1': 0.75*j.size, 'x2': j.x + 6, 'y2': j.y + 4, 'z2': 0.85*j.size, 'contour': False},
        {'x1': j.x + 10, 'y1': j.y - 4, 'z1': 0.75*j.size, 'x2': j.x + 6, 'y2': j.y - 4, 'z2': 0.85*j.size, 'contour': False},
        {'x1': j.x + 10, 'y1': j.y + 4, 'z1': 0.75*j.size, 'x2': j.x + 6, 'y2': j.y + 4, 'z2': 0.85*j.size, 'contour': False},
        #bras gauche
        {'x1': j.x - 10, 'y1': j.y - 4, 'z1': 0.4*j.size, 'x2': j.x - 10, 'y2': j.y + 4, 'z2': 0.85*j.size, 'contour': True},
        {'x1': j.x - 7, 'y1': j.y - 4, 'z1': 0.4*j.size, 'x2': j.x - 7, 'y2': j.y + 4, 'z2': 0.85*j.size, 'contour': True},
        {'x1': j.x - 10, 'y1': j.y - 4, 'z1': 0.4*j.size, 'x2': j.x - 7, 'y2': j.y - 4, 'z2': 0.85*j.size, 'contour': True},
        {'x1': j.x - 10, 'y1': j.y + 4, 'z1': 0.4*j.size, 'x2': j.x - 7, 'y2': j.y + 4, 'z2': 0.85*j.size, 'contour': True},
        #bras droit
        {'x1': j.x + 10, 'y1': j.y - 4, 'z1': 0.4*j.size, 'x2': j.x + 10, 'y2': j.y + 4, 'z2': 0.85*j.size, 'contour': True},
        {'x1': j.x + 7, 'y1': j.y - 4, 'z1': 0.4*j.size, 'x2': j.x + 7, 'y2': j.y + 4, 'z2': 0.85*j.size, 'contour': True},
        {'x1': j.x + 10, 'y1': j.y - 4, 'z1': 0.4*j.size, 'x2': j.x + 7, 'y2': j.y - 4, 'z2': 0.85*j.size, 'contour': True},
        {'x1': j.x + 10, 'y1': j.y + 4, 'z1': 0.4*j.size, 'x2': j.x + 7, 'y2': j.y + 4, 'z2': 0.85*j.size, 'contour': True},
        #tête
        {'x1': j.x + 4, 'y1': j.y - 4, 'z1': 0.85*j.size, 'x2': j.x + 4, 'y2': j.y + 4, 'z2': j.size, 'contour': True},
        {'x1': j.x - 4, 'y1': j.y - 4, 'z1': 0.85*j.size, 'x2': j.x - 4, 'y2': j.y + 4, 'z2': j.size, 'contour': True},
        {'x1': j.x + 4, 'y1': j.y - 4, 'z1': 0.85*j.size, 'x2': j.x - 4, 'y2': j.y - 4, 'z2': j.size, 'contour': True},
        {'x1': j.x + 4, 'y1': j.y + 4, 'z1': 0.85*j.size, 'x2': j.x - 4, 'y2': j.y + 4, 'z2': j.size, 'contour': True},
        #cadre pour le pseudo
        {'x1': j.x - 4, 'y1': j.y - 4, 'z1': 1.15*j.size, 'x2': j.x + 4, 'y2': j.y - 4, 'z2': 1.25*j.size},
        {'x1': j.x - 4, 'y1': j.y - 4, 'z1': 1.15*j.size, 'x2': j.x - 4, 'y2': j.y + 4, 'z2': 1.25*j.size},
        {'x1': j.x + 4, 'y1': j.y - 4, 'z1': 1.15*j.size, 'x2': j.x + 4, 'y2': j.y + 4, 'z2': 1.25*j.size},
        {'x1': j.x - 4, 'y1': j.y + 4, 'z1': 1.15*j.size, 'x2': j.x + 4, 'y2': j.y + 4, 'z2': 1.25*j.size}]
    