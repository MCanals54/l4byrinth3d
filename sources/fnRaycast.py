from pygame import draw
from math import cos,sin, radians
from divers import distancePoints, coo_to_case
dist = distancePoints
screen_ = None
ratio = None
taille_case_x = 200
taille_case_y = 200

def sign(number):
    """
    sign determine si le nombre est positif ou negatif
    :param number: un nombre (int;float)
    :return: 1 si positif ; -1 si negatif
    """
    if number < 0: return -1
    else: return 1

def mur(x,y,direction, map):
    """
    mur verifie s'il y a un mur dans la case et dans la direction indiqué
    :param x,y: les coordonnees de la case
    :param direction: "nord" , "ouest" , "sud" ou "est"
    :param map: le labyrinthe 
    :return: True s'il y a un mur , False s'il y en a pas
    """
    if x >= len(map) or y >= len(map): return
    if direction == "nord": return map[y][x][0]
    if direction == "ouest": return map[y][x][1]
    if direction == "sud": return map[y][x][2]
    if direction == "est": return map[y][x][3]

def case_suivante_vect_v4(start,end):
    """
    case_suivante_vect_v4 passe a l'intersection suivante
    :param start: tuple, coordonnees du départ
    :param end: tuple, coordonnees de l'arive
    :return: tuple: point juste avant celui d'intersection (dans la même case que start)
    :return: str: la direction dans laquelle pointe le vecteur directeur de la droite qui passe par start et end
    :return: tuple: point juste après celui d'intersection (dans la case suivante)
    """

    x_start,y_start = start
    x_end,y_end = end
    x_vect = x_end - x_start
    y_vect = y_end - y_start
    rayon = [[x_start,x_vect],[y_start,y_vect]]

    x_case,y_case = (x_start//taille_case_x , y_start//taille_case_y)

    if x_start <= x_end:
        x_unite = 1
    if x_start > x_end:
        x_unite = -1

    if y_start <= y_end:
        y_unite = 1
    if y_start > y_end:
        y_unite = -1

    # x_unite et y_unite permettent de donner la direction ex: x_unite = 1 --> est

    if sign(x_unite) == -1:
        x_line = [x_case*taille_case_x,0]
    if sign(x_unite) == 1:
        x_line = [(x_case+1)*taille_case_x,0]

    if sign(y_unite) == -1:
        y_line = [y_case*taille_case_y,0]
    if sign(y_unite) == 1:
        y_line = [(y_case+1)*taille_case_y,0]

    
    # x_line et y_line correspondent a la ligne appartenant à la grille que coupe la droite qui passe par start et end

    if x_vect == 0: x_coef = 0
    else: x_coef = (rayon[0][0] - x_line[0]) / (x_line[1] - rayon[0][1])
    x_point = (rayon[0][0]+rayon[0][1]*x_coef-x_unite,rayon[1][0]+rayon[1][1]*x_coef)
    
    if y_vect == 0: y_coef = 0
    else: y_coef = (rayon[1][0] - y_line[0]) / (y_line[1] - rayon[1][1])
    y_point = (rayon[0][0] + rayon[0][1]*y_coef , rayon[1][0] + rayon[1][1]*y_coef-y_unite)

    # x_point et y_point correspondent aux points dont les coordonnees sont dans la meme case que le start mais qui est sur le bord de la case (juste avant l'intersection entre la droite et la grille) (les deux veriables sont des tuple (coordonnees))

    if dist(start,x_point) < dist(start,y_point):
        if sign(x_vect) == 1: return x_point,"est",[rayon[0][0]+rayon[0][1]*x_coef+x_unite,rayon[1][0]+rayon[1][1]*x_coef]
        if sign(x_vect) == -1: return x_point,"ouest",[rayon[0][0]+rayon[0][1]*x_coef+x_unite,rayon[1][0]+rayon[1][1]*x_coef]

    if dist(start,x_point) > dist(start,y_point):
        if sign(y_vect) == 1: return y_point,"sud",[rayon[0][0] + rayon[0][1]*y_coef , rayon[1][0] + rayon[1][1]*y_coef+y_unite]
        if sign(y_vect) == -1: return y_point,"nord",[rayon[0][0] + rayon[0][1]*y_coef , rayon[1][0] + rayon[1][1]*y_coef+y_unite]
    
    if dist(start,x_point) == dist(start,y_point):
        if sign(x_vect) == 1: return (x_point[0],y_point[1]),"est",[rayon[0][0]+rayon[0][1]*x_coef+x_unite,rayon[1][0] + rayon[1][1]*y_coef-y_unite]
        if sign(x_vect) == -1: return (x_point[0],y_point[1]),"ouest",[rayon[0][0]+rayon[0][1]*x_coef+x_unite,rayon[1][0] + rayon[1][1]*y_coef-y_unite]
        if sign(y_vect) == 1: return (x_point[0],y_point[1]),"sud",[rayon[0][0]+rayon[0][1]*x_coef+x_unite,rayon[1][0] + rayon[1][1]*y_coef-y_unite]
        if sign(y_vect) == -1: return (x_point[0],y_point[1]),"nord",[rayon[0][0]+rayon[0][1]*x_coef+x_unite,rayon[1][0] + rayon[1][1]*y_coef-y_unite]
        
def rayon_dist(start,end, map):
    """
    rayon_dist permet d'obtenir, la distance , la direction et la case dans laquelle il y a un mur
    :param start: tuple, coordonné du point de depar du rayon (joueur)
    :parm end: point qui suit la direction dans laquelle regarde le joueur mais loin de lui (pour obtenir le vecteur dans case_suivante_v4)
    :parma map: le labyrinthe
    :return: float, distance entre start (le joueur) et le mur le plus proche dans la diection du rayon
    :return: str, direction dans laquelle se trouve le mur (nord, sud, est, ouest)
    :return: tuple, coordonnees de la case dans laquelle se trouve le mur le plus proche
    """
    global screen_, ratioRay
    start_init = start
    
    for i in range(max(len(map[0])+1,len(map))+1):
        start_case,intersection,start = case_suivante_vect_v4(start,end)
        x_case,y_case = coo_to_case(start_case[0],start_case[1])
        if mur(int(x_case),int(y_case),intersection, map):
            if ratioRay != None: draw.line(screen_, start_pos=(start_init[0]*ratioRay, start_init[1]*ratioRay), end_pos=(start[0]*ratioRay,start[1]*ratioRay), color="green")
            return dist(start_init,start),intersection,(x_case,y_case)

def partition(T):
    """
    partition permet d'obtenir 2 tableaux triés par rapport à un pivot
    :param T: tuple de trois elements
    :return: tuple de 3 elements (liste d'elements plus petit que le pivot | le pivot | liste d'elements plus grand que le pivot)
    """
    pivot = T[0][0]
    pivot_ = T[0]
    Tinf = []
    Tsup = []
    for i,val in enumerate(T):
        if i == 0:
            continue
        if val[0] <= pivot: Tinf.append(val)
        else: Tsup.append(val)
        
    return (Tinf,pivot_,Tsup)

def tri_rapide_mur(T):
    """
    tri_rapide_mur est une application du tri rapide vu en classe qui s'adapte pour trier le tuple retourné par rayon_dist (les tuples sont triés par distance, on ne s'occupe pas des autres données)
    :param T: tuple dont le 1er element est une distance
    :return: le tuple d'entré trier par rapport au premier element
    """
    if len(T) <= 1: 
        return T
    else:
        Tinf,pivot,Tsup = partition(T)
        return tri_rapide_mur(Tinf) + [pivot] + tri_rapide_mur(Tsup)
    
def raycast(start, screen, orientation, map, nb_rayon=30,ecart=3,ratio=None):
    """
    raycast est la fonction qui gère tout les rayons qui sont envoyés et qui permet de récupérer les obstacles que les rayons rencontrent
    :param start: le coordonnees du joueur
    :param screen: variable de pygame (pour faire des affichages de tests)
    :param orientation: direction dans laquelle commence le balaillage avec les rayons
    :param map: le labyrinthe
    param ratio: ratio de la taille du labyrinthe par rapport à la taille de l'écran (utile pour afficher les rayons sur la modélisation 2D) (si ratio = None c'est que l'on ne souhaite pas afficher les rayons)
    :return: liste qui contient la dirtection dans laquelle se trouve le mur touché par le rayon et la case de ce même mur ( ex: [("ouest", (2,1)), ("est", (2,1)), ("ouest", (8,3))] )
    """
    global screen_, ratioRay
    ratioRay = ratio
    screen_ = screen
    x_start,y_start = start
    o = orientation
    dist_list = []
    for i in range(nb_rayon):
        end = x_start + cos(radians(o)) * 10000 , y_start + sin(radians(o)) * 10000
        dist = rayon_dist(start,end,map)
        if not (dist == None or None in dist): dist_list.append(dist)
        o += ecart

    #creation d'une liste (brute) d'informations (dist_list)

    dist_list = tri_rapide_mur(dist_list)
    list_case = []
    list_ = []
    for vals in dist_list:
        direction_ = vals[1]
        case_ = vals[2]
        if not (direction_,case_) in list_case: # trie pour qu'un mur n'apparaisse qu'une fois dans la liste
            list_.append(vals)
            list_case.append((direction_,case_))

    list_ = list_[::-1]
    lst_final = []
    for dist_,direction_,case_ in list_:
        lst_final.append((direction_,case_,dist_))
        
    return lst_final