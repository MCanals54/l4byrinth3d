from network import Serveur
from createLab import createLaby
import threading
import bot

clients = {}

def client_recv(id):
    """
    client_recv envoie à tout les autres joueurs les donnees reçu depuis un client
    :param id: precise le client dont le serveur reçoit l'information
    """
    global clients
    while True:
        recv = clients[id].recv()
        for num_client in clients:
                if num_client == id:
                    continue
                else:
                    clients[num_client].send(recv)

def mainServ(nbPlayer, labSize:int, nameServer,addr_list, PVP): # creation et etablissement des connexion
    """
    mainServ permet de creer les differentes connexions avec les clients
    :param nbPlayer: nombre de joueurs attendus dans le serveur
    :param labSize: la taille du labyrinthe (le labyrinthe est carre donc labSize correspond à la taille d'un côté)
    :param nameServer: le nom du serveur, format: "nom_du_serveur:ip_du_serveur"
    """
    global clients
    nb_connection = 0
    client_list = []
    dict_pseudo = {}

    for num_client in range(nbPlayer): # ouvre les differents ports pour que les client s'y connectent
        
        bot.change_server_addr(nameServer,addr_list[num_client])

        clients[num_client] = Serveur(4440+num_client)
        client_list.append(clients[num_client])
        nb_connection += 1
        print(clients[num_client].recv()) # Client n connected
        clients[num_client].send(f"{nbPlayer}")
        print(clients[num_client].recv()) # nombre de joueur reçu
        clients[num_client].send(str(num_client))
        print(clients[num_client].recv()) # id du joueur reçu

        clients[num_client].send(str(PVP))
        print(clients[num_client].recv()) # mode PVP reçu

        if num_client != 0: 
            for k in dict_pseudo.keys():
                clients[num_client].send(dict_pseudo[k])
                print(clients[num_client].recv()) # pseudo reçu
        
        pseudo = clients[num_client].recv()
        dict_pseudo[num_client] = pseudo

        for c in client_list[0:-1]:
            c.send(str(nb_connection)+pseudo)
            print(c.recv())

    print("fini de créer les clients")

    bot.supr_server(nameServer)
    print("le nom de serveur a été suprimé de la liste")
    grilleLab, case_sortie = createLaby(int(labSize), PVP)
    for c in client_list: # envoie le labyrinthe et les pseudo aux clients
        c.send_lab(grilleLab)
        print(c.recv())
        c.send_case_sortie(case_sortie)
        print(c.recv())
        c.send_pseudo(dict_pseudo)
        print(c.recv())
  

    for num_client in range(nbPlayer): # la fonction client_recv est lancé en parallèle pour chaques clients
        threading.Thread(target=client_recv, args=(num_client,)).start()

if __name__ == "__main__": # ne s'execute que si lab3D_serveur.py n'est pas utilisé dans un autre fichier (import) | (c'est pour les tests)
    mainServ(2)