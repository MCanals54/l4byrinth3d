from math import pi, atan2, sqrt, radians, cos, sin, degrees
import threading
import pygame

def draw_text(surface, text:str, font, color, x,y):
    img = font.render(text, True, color)
    surface.blit(img, (x,y))

def calculAngle(A,B,C) -> float:
    """calcule l'ange ABC (sens trigonométrique)
       :param A,B,C: chacun représente un point (donc un tuple de float)
       :return: l'angle en degré (float)
    """
    xA, yA = A[0:2]
    xB, yB = B[0:2]
    xC, yC = C[0:2]
    return (degrees(atan2(yC - yB, xC - xB) - atan2(yA - yB, xA - xB)) + 360) % 360

def distancePoints(xy1, xy2)->float:
    """calcule la disatnce entre 2 points
       :param xy1, xy2: chacun représente un point (donc un tuple de float)
       :return: la distance (float)
    """
    x1,y1 = xy1
    x2,y2 = xy2
    return sqrt((x2-x1)**2+(y2-y1)**2)

def creer_un_thread(cible):
    """creer un thread (exécute un code en simultané du reste du code)
       :param cible: callback
    """
    thread = threading.Thread(target=cible)
    thread.daemon = True
    thread.start()

def coo_to_case(x,y):
    return (x//200 , y//200)

def cooAbsoluesMurs(listeMurs, case_sortie = None):
    """
    cooAbsoluesMurs transforme les coordonnées d'un mur dans la grille du labyrinthe (côté, x, y) en coordonnées absolues du labyrinthe 3D de 4 murs (pour créer une impression de mur avec une épaisseur on créer en réalité 4 murs sans épaisseur)
    :param listeMurs: liste du format suivant [(côté, x, y), (côté, x, y), (côté, x, y)] (côté est une chaine de caractère égale à "nord", "ouest', "est" ou "sud" et x et y sont deux entiers)
    :param case_sortie: tuple d'entier représentant la case de sortie du labyrinthe (si case_sortie == None, c'est qu'on ne cherche pas à vérifier que certains murs constituent la case de sortie)
    :return: liste de dictionnaire de murs tel que [{"x1": int, "x2": int, "y1": int, "y2": int, "v": bool, "sortie": bool},...] ("v" = True signifie que le mur est vertical)
    """
    murs = []
    for m in listeMurs:
        if type(m) == int: murs.append(m)
        else:
            direct = m[0]
            mx,my = m[1]
            if direct == "nord":
                murs.append({"x1": mx * 200, "x2": (mx + 1) * 200, "y1": my * 200 - 5, "y2": my * 200 - 5, "v":False, "sortie": (my,mx) == case_sortie})
                murs.append({"x1": mx * 200, "x2": (mx + 1) * 200, "y1": my * 200 + 5, "y2": my * 200 + 5, "v":False, "sortie": (my,mx) == case_sortie})
                murs.append({"x1": mx * 200, "x2": mx * 200, "y1": my * 200 - 5, "y2": my * 200 + 5, "v":True, "sortie": (my,mx) == case_sortie})
                murs.append({"x1": (mx + 1) * 200, "x2": (mx + 1) * 200, "y1": my * 200 - 5, "y2": my * 200 + 5, "v":True, "sortie": (my,mx) == case_sortie})
            elif direct == "sud":
                murs.append({"x1": mx * 200, "x2": (mx + 1) * 200, "y1": (my+1) * 200 - 5, "y2": (my+1) * 200 - 5, "v":False, "sortie": (my,mx) == case_sortie})
                murs.append({"x1": mx * 200, "x2": (mx + 1) * 200, "y1": (my+1) * 200 + 5, "y2": (my+1) * 200 + 5, "v":False, "sortie": (my,mx) == case_sortie})
                murs.append({"x1": mx * 200, "x2": mx * 200, "y1": (my+1) * 200 - 5, "y2": (my+1) * 200 + 5, "v":True, "sortie": (my,mx) == case_sortie})
                murs.append({"x1": (mx + 1) * 200, "x2": (mx + 1) * 200, "y1": (my+1) * 200 - 5, "y2": (my+1) * 200 + 5, "v":True, "sortie": (my,mx) == case_sortie})
            elif direct == "ouest":
                murs.append({"x1": mx * 200 - 5, "x2": mx * 200 - 5, "y1": my * 200, "y2": (my+1) * 200, "v":True, "sortie": (my,mx) == case_sortie})
                murs.append({"x1": mx * 200 + 5, "x2": mx * 200 + 5, "y1": my * 200, "y2": (my+1) * 200, "v":True, "sortie": (my,mx) == case_sortie})
                murs.append({"x1": mx * 200 - 5, "x2": mx * 200 + 5, "y1": my * 200, "y2": my * 200, "v":False, "sortie": (my,mx) == case_sortie})
                murs.append({"x1": mx * 200 - 5, "x2": mx * 200 + 5, "y1": (my+1) * 200, "y2": (my+1) * 200, "v":False, "sortie": (my,mx) == case_sortie})
            elif direct == "est":
                murs.append({"x1": (mx+1) * 200 - 5, "x2": (mx+1) * 200 - 5, "y1": my * 200, "y2": (my+1) * 200, "v":True, "sortie": (my,mx) == case_sortie})
                murs.append({"x1": (mx+1) * 200 + 5, "x2": (mx+1) * 200 + 5, "y1": my * 200, "y2": (my+1) * 200, "v":True, "sortie": (my,mx) == case_sortie})
                murs.append({"x1": (mx+1) * 200 - 5, "x2": (mx+1) * 200 + 5, "y1": my * 200, "y2": my * 200, "v":False, "sortie": (my,mx) == case_sortie})
                murs.append({"x1": (mx+1) * 200 - 5, "x2": (mx+1) * 200 + 5, "y1": (my+1) * 200, "y2": (my+1) * 200, "v":False, "sortie": (my,mx) == case_sortie})
    return murs

def rotation(xy, C, angle_degrees):
    # Conversion de l'angle de degrés à radians
    x,y = xy
    cx,cy = C
    angle_radians = radians(angle_degrees)
    
    # Calcul des distances du point par rapport au centre
    dx = x - cx
    dy = y - cy
    
    # Calcul des nouvelles coordonnées après rotation
    new_x = cx + dx * cos(angle_radians) - dy * sin(angle_radians)
    new_y = cy + dx * sin(angle_radians) + dy * cos(angle_radians)

    return (new_x,new_y)