from math import *
from pygame import *
from divers import *
from class_gun import Gun


class Joueur():
    def __init__(self,x,y,idJ,orientationHorizontale,orientationVerticale,taille,vitesse,size,map,sensi=4,pseudo=""):
        self.x = x # abscisses du joueur dans le plan 2D et 3D
        self.y = y # ordonnées du joueur dans le plan 2D et 3D
        self.id = idJ # id du joueur (utile pour le multijoueur)
        self.orientationHorizontale = orientationHorizontale # orientation horizontale du joueur
        self.orientationVerticale = orientationVerticale # orienttation verticale du joueur
        self.taille = taille # taille du rond de la représentation 2D et du rayon pour les collisions
        self.vitesse = vitesse # vitesse de déplacement du joueur
        self.size = size # taille du joueur (important pour la représentation 3D du champ de vision)
        self.map = map # grille de labyrinthe dans laquelle se déplace le joueur (voire la structure dans genererGrilleLab() dans createLaby.py)
        self.sensi = sensi # la sensibilité de la vue du joueur
        self.pseudo = pseudo # le pseudo du joueur (aucun en mode solo)
        self.vie = 100 # utile pour le mode PVP
        self.gun = Gun(5)  # l'arme du joueur dans le mode PVP
        self.nbEliminations = 0 # compte du nombre d'éliminations du joueur dans le mode PVP

    def coo_to_case(self,x:float,y:float)-> list:   
        """
        coo_to_case donne les coordonnées de la case pour les coordonnees absolue de la fenetre d'un point
        :param x: l'abscisse du point en coordonnées absolues de la fenêtre d'un point
        :param y: l'ordonnée du point en coordonnées absolues de la fenêtre d'un point
        :return: les coordonnées de la case 
        """
        
        return [x//200 , y//200] #200 étant la taille d'une case

    def drawPlayer2D(self, screen:Surface, col="grey", ratio=1.0): #dessin en 2D du joueur (pour les tests)
        """
        drawPlayer2D dessine le joueur en 2 dimensions avec son champ de vision (90°)
        :param Surface: là où l'on dessine le joueur
        :param col: la couleur du rond représentant le joueur
        :param ratio: ratio de la taille du labyrinthe par rapport à la taille de l'écran
        :return: void
        """
        x,y,tailleJoueur,oriH = self.x, self.y, self.taille*ratio, self.orientationHorizontale

        draw.circle(screen, color=col, center=(x*ratio, y*ratio), radius=tailleJoueur)
   
        draw.line(screen, start_pos=(x*ratio, y*ratio), end_pos=((x + cos(radians(oriH+45))*180)*ratio, (y + sin(radians(oriH+45))*180)*ratio), color="blue")
        draw.line(screen, start_pos=(x*ratio, y*ratio), end_pos=((x + cos(radians(oriH-45))*180)*ratio, (y + sin(radians(oriH-45))*180)*ratio), color="red")
        draw.line(screen, start_pos=(x*ratio, y*ratio), end_pos=((x + cos(radians(oriH))*180)*ratio, (y + sin(radians(oriH))*180)*ratio), color="blue")

    def verif_direction(self,x1,y1, autres_joueurs={}): # collision avec les murs
        """
        verif_direction verifie si un déplacement du joueur possible (pour cela on prend en considération les murs d'uniquement les cases voisines de ma case actuelle du joueur)
        :param x1,y1: les coordonnées après le deplacement
        :param autres_joueurs: dict qui assoscie l'id d'un joueur à celui-ci -> permet de s'empêcher les colisions avec les autres joueurs
        :return: booleen -> True: déplacement possible
        """
        x,y = self.x, self.y
        mazeSide = len(self.map[0])
        caseX, caseY = self.coo_to_case(x,y)
        caseX, caseY = int(caseX), int(caseY)

        murs = [] 
        if caseX == 0: murs_a_ajouter = [[caseX, caseX+1]]
        elif caseX == mazeSide-1: murs_a_ajouter = [[caseX-1, caseX]]
        else: murs_a_ajouter = [[caseX-1, caseX, caseX+1]]

        if caseY == 0: murs_a_ajouter.append([caseY, caseY+1])
        elif caseY == mazeSide-1: murs_a_ajouter.append([caseY-1, caseY])
        else: murs_a_ajouter.append([caseY-1, caseY, caseY+1])
    
        directions = ["nord", "ouest", "sud", "est"]

        # on récupère les murs des cases voisines
        for x in murs_a_ajouter[0]:
            for y in murs_a_ajouter[1]:
                for i, mur in enumerate(self.map[y][x]):
                    if mur: murs.append((directions[i], (x,y)))

        murs = cooAbsoluesMurs(murs)
        
        # on vérifie si le joueur ne traverse pas un mur
        for mur in murs:
            if not mur["v"]:
                if x1 < mur["x2"] and x1 > mur["x1"]:
                    if distancePoints((x1, mur["y1"]), (x1, y1)) < self.taille: return False
                elif y1 > mur["x2"] and distancePoints((mur["x2"], mur["y1"]), (x1, y1)) < self.taille: return False
                elif y1 < mur["x1"] and distancePoints((mur["x1"], mur["y1"]), (x1, y1)) < self.taille: return False
            else:
                if y1 < mur["y2"] and y1 > mur["y1"]:
                    if distancePoints((mur["x1"], y1), (x1, y1)) < self.taille: return False
                elif y1 > mur["y2"] and distancePoints((mur["x1"], mur["y2"]), (x1, y1)) < self.taille: return False
                elif y1 < mur["y1"] and distancePoints((mur["x1"], mur["y1"]), (x1, y1)) < self.taille: return False

        if autres_joueurs != {}:
            for jID in autres_joueurs.keys():
                j = autres_joueurs[jID]
                distJ = dist((x1, y1), (j.x, j.y))
                if distJ < self.taille or distJ < j.taille: return False
        return True

    def avancer(self, autres_joueurs={}):
        """
        avancer change les coordonnees du joueur (vers l'avant par rapport à là où il regarde)
        :param autres_joueurs: dict qui assoscie l'id d'un joueur à celui-ci -> permet de s'empêcher les colisions avec les autres joueurs
        :return: modifie self, effet de bord
        """
        x = self.x + cos(radians(self.orientationHorizontale)) * self.vitesse
        y = self.y + sin(radians(self.orientationHorizontale)) * self.vitesse

        if self.verif_direction(x,y, autres_joueurs): self.x, self.y = x,y

    def reculer(self, autres_joueurs={}):
        """
        reculer change les coordonnees du joueur (vers l'arrière par rapport à là où il regarde)
        :param autres_joueurs: dict qui assoscie l'id d'un joueur à celui-ci -> permet de s'empêcher les colisions avec les autres joueurs
        :return: modifie self, effet de bord
        """
        x = self.x - cos(radians(self.orientationHorizontale)) * self.vitesse
        y = self.y - sin(radians(self.orientationHorizontale)) * self.vitesse

        if self.verif_direction(x,y, autres_joueurs): self.x, self.y = x,y
        
    def droite(self, autres_joueurs={}): #le joueur se déplace à droite sans changer son orientation
        """
        droite change les coordonnees du joueur (vers la droite par rapport à là où il regarde)
        :param autres_joueurs: dict qui assoscie l'id d'un joueur à celui-ci -> permet de s'empêcher les colisions avec les autres joueurs
        :return: modifie self, effet de bord
        """
        x = self.x + cos(radians(self.orientationHorizontale+90)) * self.vitesse
        y = self.y + sin(radians(self.orientationHorizontale+90)) * self.vitesse

        if self.verif_direction(x,y, autres_joueurs): self.x, self.y = x,y
        
    def gauche(self, autres_joueurs={}): #le joueur se déplace à gauche sans changer son orientation
        """
        gauche change les coordonnees du joueur (vers la gauche par rapport à là où il regarde)
        :param autres_joueurs: dict qui assoscie l'id d'un joueur à celui-ci -> permet de s'empêcher les colisions avec les autres joueurs
        :return: modifie self, effet de bord
        """
        x = self.x + cos(radians(self.orientationHorizontale-90)) * self.vitesse
        y = self.y + sin(radians(self.orientationHorizontale-90)) * self.vitesse

        if self.verif_direction(x,y, autres_joueurs): self.x, self.y = x,y


    def tourner_droite(self, dep): 
        """
        tourner_droite modifie l'orientation horizontal du joueur (vers la droite par rapport à là où il regarde)
        :param dep: float ou int qui correspond à un déplacement de la souris 
        :return: modifie self, effet de bord
        """
        self.orientationHorizontale += self.sensi * 0.3*sqrt(dep)
    
    def tourner_gauche(self, dep): 
        """
        tourner_gauche modifie l'orientation horizontal du joueur (vers la gauche par rapport à là où il regarde)
        :param dep: float ou int qui correspond à un déplacement de la souris 
        :return: modifie self, effet de bord
        """
        self.orientationHorizontale -= self.sensi * 0.3*sqrt(dep)

    def tourner_haut(self, dep): 
        """
        tourner_haut modifie l'orientation vertical du joueur (vers le haut par rapport à là où il regarde)
        :param dep: float ou int qui correspond à un déplacement de la souris 
        :return: modifie self, effet de bord
        """
        if self.orientationVerticale <= 40: self.orientationVerticale += self.sensi*0.75 * 0.3*sqrt(dep)# <= 40   --> on ne veut pas que le joueur se retrouve avec un angle de vision qui serai imposible en vrai 
    
    def tourner_bas(self, dep): 
        """
        tourner_bas modifie l'orientation vertical du joueur (vers le bas par rapport à là où il regarde)
        :param dep: float ou int qui correspond à un déplacement de la souris 
        :return: modifie self, effet de bord
        """
        if self.orientationVerticale >= -40: self.orientationVerticale -= self.sensi*0.75 * 0.3*sqrt(dep)# >=     même chose

    def setCoo(self, xy):
        """
        setCoo modifie les coordonnees du joueur en les remplaçant par les coordonnees passées en argument
        :param xy: tuple de deux valeur numeriques (des nombres (int;float))
        :return: modifie self, effet de bord
        """
        self.x, self.y = xy

    def setOrientationH(self,oriH):
        """
        setOrientationH modifie l'orientation du joueur en les remplaçant par l'orientation passées en argument
        :param oriH: orientation (int;float)
        :return: modifie self, effet de bord
        """
        self.orientationHorizontale = oriH

    def degat(self,degat):
        """
        degat inflige des dégâts au joueur
        :param degat: int, nombre de dégâts infligé au joueur
        :return: bool, False si le joueur est mort, True s'il a survecu
        """
        self.vie -= degat
        if self.vie < 0: self.vie = 0
        if self.vie <= 0: return False
        else: return True

    def tirer(self,autres_joueurs):
        """
        tirer permet de calculer les coordonnées du point d'impact par rapport où regarde le joueur
        :param autres_joueur: liste d'objet joueur
        :return: un tuple composé de : un tuple de coordonnées, un float et l'id du joueur touché si joueur touché il y a
        :return: str, "plus de balle", s'il n'y a plus de balle
        """
        if self.gun.shot():
            print(f'nb de balles: {self.gun.nb_bullets}')
            collision_mur = False
            collision_joueur = False
            x,y = self.x,self.y
            while not collision_mur and not collision_joueur:
                x_1 = x + cos(radians(self.orientationHorizontale)) * self.vitesse
                y_1 = y + sin(radians(self.orientationHorizontale)) * self.vitesse
                
                case1 = self.coo_to_case(x,y)
                case2 = self.coo_to_case(x_1,y_1)
                if case1 != case2:
                    if case1[0] - case2[0] == 1:
                        direction = 1 #"est"
                    if case1[0] - case2[0] == -1:
                        direction = 3 #"ouest"
                    if case1[1] - case2[1] == 1:
                        direction = 0 #"sud"
                    if case1[1] - case2[1] == -1:
                        direction = 2 #"nord"
                        
                    if self.map[int(case1[1])][int(case1[0])][direction]:
                        collision_mur = True
                    
                x,y = x_1,y_1


                for jID in autres_joueurs:
                    joueur = autres_joueurs[jID]
                    if dist((x,y),(joueur.x,joueur.y)) < self.taille:
                        collision_joueur = True

            
            impact_horizontal = (x,y)
            impact_vertical = dist((self.x,self.y),(x,y)) * tan(radians(self.orientationVerticale))
            impact_vertical += self.size
            if collision_joueur: return impact_horizontal,impact_vertical,jID
            else: return impact_horizontal,impact_vertical
        else: 
            print("plus de balles")
            return "plus de balles"

    def mort(self): return self.vie == 0 # renvoie True si le joueur est mort, False sinon