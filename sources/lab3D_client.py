from network import Client
from classe_joueur import Joueur
from pygame import *
from tkinter import *
from divers import *
from customtkinter import *
import bot
from time import sleep
from start_game import startGame
i = 0
screen, grilleLab, case_sortie, joueur, autres_joueurs, server = [None] * 6
root = Tk()
HEIGHT = root.winfo_screenheight()
WIDTH = root.winfo_screenwidth()
root.destroy()

def initServer(pseudo,nameServer): # etablissement de la connexion au serveur | il faut ajouter le pseudo dans la partie graphique
    """
    initServeur permet de creer la connection avec le serveur
    :param host: str, IP du serveur
    :param nb_de_joueur_attendu: nombre maximal de connexio attendu (int)
    :return: si une connexion est etablie: list; True, objet socket correspondant au serveur, le numero du joueur, le nombre de joueurs déjà connectés
    :return: si aucune connexion est établie: list; False (il n'y a qu'un element dans la list) 
    """
    global server
    # connexion au serveur
    sleep(1)
    host = bot.addr_from_name(nameServer)
    print("\n")
    print(f'host:"{host}"')
    print("\n")
    host,port = host.split(":")
    
    print("\n"*3)
    print(f"addr:{host}\nport:{port}")
    print("\n"*3)

    try:
        server = Client(host,int(port))
    except Exception as e:
        print("\n"*3)
        print(e)
        print("\n"*3)
        return [False]

    server.send(f"Client connected") # envoie d'un msg de confirmation au serveur
    nb_de_joueur = int(server.recv()) # recupere le nombre de joueur
    server.send(f"Nombre de joueur reçu ({nb_de_joueur})")
    id_player = int(server.recv()) # num_client
    server.send("ID reçu")

    PVP = server.recv() == "True"
    server.send("Mode PVP reçu")
    
    lstPseudo = []
    if id_player != 0: 
        for i in range(id_player):
            p = server.recv()
            lstPseudo.append(p)
            server.send(f"Pseudo '{p}' reçu")

    server.send(pseudo)


    return [True, server, id_player,nb_de_joueur, lstPseudo, PVP]

def waitingServer(server, nb_de_joueur, current_widgets): # cette function s'execute pendant que le joueur attend que le serveur soit complet
    """
    waitingServer permet de recuperer l'info quand quelqu'un se connecte au serveur et l'ajoute au visuel (le fenetre)
    :param serveur: objet de socket qui correspond a la connexion avec le serveur
    :param nb_de_joueur: nombre de joueur total necessaire pour que la partie se lance
    :param current_widgets: objet de custom_tkinter
    """
    print("Début de waiting serveur")
    nb_de_connection = 0
    while nb_de_connection < nb_de_joueur:
        msg = server.recv()
        nb_de_connection = int(msg[0])
        print(nb_de_connection)
        server.send("reçu")
        addPlayerWaiting(current_widgets, nb_de_connection, nb_de_joueur, msg[1:]) # quand un joueur se connecte, l'ecran d'attente est modifié (on voit qu'un joueur s'est connecté)
    print("Fin de waiting serveur")
   
def addPlayerWaiting(current_widgets, nb_de_connection, nb_de_joueur, pseudo): # affichage graphique (ecrant d'attente)
    """
    addPlayerWaiting permet d'ajouter visuellement un joueur
    :param current_widgets: objet de custom_tkinter
    :param nb_de_connection: nombre de joueur déjà connecté au serveur
    :param nb_de_joueur: nombre de joueurs totals necessaires pour que le serveur lance la partie
    """
    print("Début addPlayerWaiting")
    texte = f"En attente des autres joueurs ({nb_de_connection}/{nb_de_joueur})"
    label1 = current_widgets["label2"]
    label1.configure(text=texte)

    texte = f"-> {pseudo}"
    print(texte)
    label2 = current_widgets[f"label{5+nb_de_connection}"]
    label2.configure(text=texte)

def creation_joueurs(nb_de_joueur,id_player, FS, pseudo, PVP):
    """
    cration_joueurs creer les objets joueurs, le joueur (controlé) et les autres joueurs
    :param nb_de_joueur: int , nombre de joueur dans la partie
    :param id_player: int , identifiant du joueur
    :param FS: bool , fullscreen ou non
    """
    global joueur, grilleLab, case_sortie, autres_joueurs
    positionInitial = {0: (26, 26), 1: (53, 26), 2: (26, 53), 3: (53, 53)}

    joueur = Joueur(positionInitial[id_player][0],positionInitial[id_player][1],id_player,0,0,20,2,100,grilleLab, pseudo=pseudo[str(id_player)])
    autres_joueurs = {}
    for i in range(nb_de_joueur):
        if i == id_player: continue
        else: autres_joueurs[i] = Joueur(positionInitial[i][0],positionInitial[i][1],i,0,0,20,2,100,grilleLab, pseudo=pseudo[str(i)])
            

    print("Création des joueurs terminés --> main()")
    startGame(FS, solo=False, PVP=PVP, data= {"grilleLab": grilleLab, "case_sortie": case_sortie, "joueur": joueur, "autres_joueurs": autres_joueurs, "server": server})

def recv_lab_pseudo(server,nb_de_joueur,id_player, FS, PVP): # reçoit le labyrinthe et les pseudo depuis le serveur
    """
    recv_lab est la fonction qui permet de recevoir le labyrinthe du serveur
    :param server: objet socket qui correspond à la connection avec le serveur
    :param nb_de_joueur: nombre de joueurs totals necessaires pour que le serveur lance la partie
    :param id_player: numero du joueur
    """
    global grilleLab, case_sortie
    grilleLab = server.recv_lab()
    server.send("lab recv")

    case_sortie = server.recv_case_sortie()
    server.send("case sortie recv")
    case_sortie = (int(case_sortie[0]), int(case_sortie[1]))
    
    dict_pseudo = server.recv_pseudo() 
    server.send('pseudo recv')
    print(dict_pseudo)
    print("J'ai reçu le labyrinthe -> création du joueur")
    creation_joueurs(nb_de_joueur,id_player, FS, dict_pseudo, PVP)

