**rôle de chaque fichier du dossier source**

- dossier `src` : dossier contenant les images et les sons. <br>
- `bot.py` : contient les fonctions qui gèrent les interactions avec le site internet qui nous permet d'utiliser un pseudo système de DNS
- `classe_joueur.py` : définition de la classe joueur et de ses méthodes 
- `class_gun.py` : définition de la classe gun et de ses méthodes 
- `convert3D.py` : toutes les fonctions qui gèrent la modélisation 3D
- `createLab.py` :  toutes les fonctions qui permettent de générer aléatoirement les grilles de labyrinthe 
- `divers.py` :  il s'agit de fonctions simples et passe-partout qui nous sont utiles un peu partout 
- `draw_lab.py` : fonctions qui gèrent l'affichage durant la partie. La fonction drawLab() est celle qui est appelé à chaque itération de la boucle pygame et qui permet de tout dessiner à l'écran 
- `fnRaycast.py` :  il s'agit de toutes les fonctions à propos du raycast.
- `index.txt` : fichier texte qui nous sert à récupérer les informations sur les serveurs ouverts, il est donc essentiel pour la résolution DNS
- `lab3D_client.py` :  contient les fonctions qui gèrent l'envoi des données du joueur au serveur et la réception des informations du serveur lors de la création de la partie et créer les objets joueurs.
- `lab3D_serveur.py`:  contient les fonctions qui permettent au serveur de recevoir les données des différents clients et de les renvoyer lors de la création de la partie.
- `main.py` : fichier de lancement du programme principal. Gère les différents menus et lance la boucle de tkinter.
- `network.py` : defini les classes Serveur et Client ainsi que toutes les fonctions que l'on utilise pour établir une liaison, la réception et l'envoi de données pour le multijoueur
- `skin.py` : les différentes représentations représentations des joueurs en 3D (il n'y en a que 2 pour l'instant)
- `start_game.py` :  c'est là qu'on intialise les variables pour la partie, qu'on crée les pygame widgets nécessaires et on lance la partie avec la boucle principale
